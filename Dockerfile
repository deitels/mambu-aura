FROM ubuntu:xenial

RUN apt-get update && apt-get -y install rsync libgmp10 libbz2-1.0 zlib1g && \
    rm -rf /var/lib/apt/lists/*

ADD ["aura++", "run.sh", "ip-to-country.csv", "/opt/aura-bot/"]

ADD ["libstorm.so", "libstorm.so.9", "libstorm.so.9.21.0", "libbncsutil.so", "/usr/lib/"]

RUN ln -s /data/aura.cfg /opt/aura-bot/aura.cfg

WORKDIR /opt/aura-bot/

EXPOSE 6112

EXPOSE 6112/udp

CMD ["bash", "/opt/aura-bot/run.sh"]
